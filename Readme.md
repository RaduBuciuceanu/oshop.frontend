## OShop.Frontend
It's the frontend app for OShop system. It should have a running backend. It uses:
* React
* Railway programming (rxjs)
* Redux
* Redux-Observable

![docs/presentation.png](docs/presentation.png "Presentation image")

### Dependencies
* A running backend
* Node.js

### Start
> You should have yarn package manager to be installed.

> You should have a running backend instance.

1. Install all dependencies: `yarn install`
2. Start the application: `yarn start`
3. Access the website: http://localhost:3000/

### All articles
The view where we can view all the existent articles. It can be accesses here: 
* http://localhost:3000/articles/all

![docs/all-articles.png](docs/all-articles.png "All articles sequence diagram")

### Create ball
The view where we can create a new ball. It can be accessed here: 
* http://localhost:3000/balls/create

![docs/create-ball.png](docs/create-ball.png "Create ball sequence diagram")

### Create rollers
The view where we can create a new rollers. It can be accessed here: 
* http://localhost:3000/rollers/create

![docs/create-rollers.png](docs/create-rollers.png "Create rollers sequence diagram")
