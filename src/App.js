import React from 'react'
import { Route, Router, Switch } from 'react-router-dom'
import { createBrowserHistory } from 'history'
import { Provider } from 'react-redux'

import 'src/App.css'
import Layout from 'src/layout/containers/Layout'
import ReduxStoreBuilder from 'src/core/redux/redux-store-builder'

const browserHistory = createBrowserHistory()
const store = new ReduxStoreBuilder().withHistory(browserHistory).build()

class App extends React.Component {
  render () {
    return <Provider store={store}>
      <Router history={browserHistory}>
        <Switch>
          <Route exact path={'/*'} component={Layout}/>
        </Switch>
      </Router>
    </Provider>
  }
}

export default App
