import {GET_ALL_ACTIONS, GET_ALL_ACTIONS_FULFILLED} from 'src/articles/actions/constants';

export const getAllArticles = () => ({type: GET_ALL_ACTIONS})

export const getAllArticlesFulfilled = (all) => ({type: GET_ALL_ACTIONS_FULFILLED, payload: all})
