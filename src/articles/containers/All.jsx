import React from 'react'
import {connect} from 'react-redux'
import * as PropTypes from 'prop-types'
import {bindActionCreators} from 'redux'
import ReactTable from 'react-table'
import {Grid, Button, Typography} from '@material-ui/core'

import {getAllArticles} from 'src/articles/actions'

class All extends React.Component {
    componentWillMount() {
        this.props.getAllArticles()
    }

    render() {
        const {all} = this.props

        return <Grid container justify={'center'} alignItems={'center'} spacing={8}>
            <Grid item lg={10}>
                <Typography variant={'h4'} align={'center'}>All articles</Typography>
            </Grid>
            <Grid item lg={10}>
                <ReactTable columns={this.#buildTableColumns()} data={all} defaultPageSize={18}
                            className="-striped -highlight"/>
            </Grid>
        </Grid>
    }

    #buildTableColumns = () => ([
        {
            Header: <Button fullWidth>Id</Button>,
            accessor: 'id'
        },
        {
            Header: <Button fullWidth>Name</Button>,
            accessor: 'name'
        },
        {
            Header: <Button fullWidth>Description</Button>,
            accessor: 'description'
        },
        {
            Header: <Button fullWidth>Price</Button>,
            accessor: 'price'
        }
    ])
}

All.propTypes = {
    all: PropTypes.array.isRequired,
    getAllArticles: PropTypes.func.isRequired
}

const mapStateToProps = ({articles}) => ({all: articles.all})

const mapDispatchToProps = (dispatch) => bindActionCreators({getAllArticles}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(All)
