import {ofType} from 'redux-observable'
import {tap, map, switchMap} from 'rxjs/operators'
import {ajax} from 'rxjs/ajax'

import {GET_ALL_ACTIONS} from 'src/articles/actions/constants'
import {getAllArticlesFulfilled} from 'src/articles/actions'

const url = 'http://localhost:5000/api/article/all'

export const articles = (actions$) => actions$.pipe(ofType(GET_ALL_ACTIONS))
    .pipe(switchMap(() => ajax.get(url)))
    .pipe(map((ajaxResponse) => getAllArticlesFulfilled(ajaxResponse.response)))
