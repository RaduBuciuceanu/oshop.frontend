import {Record} from 'immutable'

import {GET_ALL_ACTIONS_FULFILLED} from 'src/articles/actions/constants'

const Article = Record({id: null, name: null, description: null, price: null})
const State = Record({all: []})

export const articles = (state = new State().toJS(), action) => {
    if (action.type === GET_ALL_ACTIONS_FULFILLED) {
        return getAllActionsFulfilled(state, action.payload)
    }

    return state
}

const getAllActionsFulfilled = (state, all) => new State({...state, all: all.map(item => new Article(item))}).toJS()
