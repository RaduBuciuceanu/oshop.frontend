import {CREATE_BALL, CREATE_BALL_FULFILLED} from 'src/balls/actions/constants'

export const createBall = (ball) => ({type: CREATE_BALL, payload: ball})
export const createBallFulfilled = (createdBall) => ({type: CREATE_BALL_FULFILLED, payload: createdBall})
