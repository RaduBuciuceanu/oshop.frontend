import React from 'react'
import {TextField, Button, Grid, Typography} from '@material-ui/core'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import * as PropTypes from 'prop-types'

import {createBall} from 'src/balls/actions'

class Create extends React.Component {
    state = {ball: {}}

    render() {
        const {createBall} = this.props
        const {ball} = this.state

        return <Grid container alignItems={'center'} justify={'center'} spacing={2}>
            <Grid item lg={7} md={12} sm={12} xs={12}>
                <Typography variant={'h4'} align={'center'}>Create balls</Typography>
            </Grid>
            <Grid item lg={7} md={12} sm={12} xs={12}>
                <TextField label={'Name'} fullWidth onChange={this.#changeName}/>
            </Grid>
            <Grid item lg={7} md={12} sm={12} xs={12}>
                <TextField label={'Description'} fullWidth onChange={this.#changeDescription}/>
            </Grid>
            <Grid item lg={7} md={12} sm={12} xs={12}>
                <TextField label={'Price'} fullWidth onChange={this.#changePrice}/>
            </Grid>
            <Grid item lg={7} md={12} sm={12} xs={12}>
                <TextField label={'Maximum pressure'} fullWidth onChange={this.#changeMaximumPressure}/>
            </Grid>
            <Grid item lg={7} md={12} sm={12} xs={12}>
                <TextField label={'Diameter'} fullWidth onChange={this.#changeDiameter}/>
            </Grid>
            <Grid item lg={7} md={12} sm={12} xs={12}>
                <Button fullWidth onClick={() => createBall(ball)}>Create</Button>
            </Grid>
        </Grid>
    }

    #changeDiameter = (event) => {
        this.setState({ball: {...this.state.ball, diameter: event.target.value}})
    }

    #changeMaximumPressure = (event) => {
        this.setState({ball: {...this.state.ball, maximumPressure: event.target.value}})
    }

    #changePrice = (event) => {
        this.setState({ball: {...this.state.ball, price: event.target.value}})
    }

    #changeDescription = (event) => {
        this.setState({ball: {...this.state.ball, description: event.target.value}})
    }

    #changeName = (event) => {
        this.setState({ball: {...this.state.ball, name: event.target.value}})
    }
}

Create.propTypes = {
    createBall: PropTypes.func.isRequired
}

const mapStateToProps = ({balls}) => ({})

const mapDispatchToProps = (dispatch) => bindActionCreators({createBall}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(Create)
