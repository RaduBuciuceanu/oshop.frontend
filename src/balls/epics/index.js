import {ofType} from 'redux-observable'
import {switchMap, map} from 'rxjs/operators'
import {ajax} from 'rxjs/ajax'

import {CREATE_BALL} from 'src/balls/actions/constants'
import {createBallFulfilled} from 'src/balls/actions'

const url = 'http://localhost:5000/api/ball/create'

const headers = {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
}

export const balls = (actions$) => actions$.pipe(ofType(CREATE_BALL))
    .pipe(switchMap((action) => ajax.post(url, action.payload, headers)))
    .pipe(map((ajaxResponse) => createBallFulfilled(ajaxResponse.response)))
