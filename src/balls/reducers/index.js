import {Record} from 'immutable'

import {CREATE_BALL_FULFILLED} from 'src/balls/actions/constants'

const Ball = Record({id: null, name: null, description: null, price: null, maximumPressure: null, diameter: null})
const State = Record({all: []})

export const balls = (state = new State().toJS(), action) => {
    if (action.type === CREATE_BALL_FULFILLED) {
        return createBallFulfilled(state, action.payload)
    }

    return state
}

const createBallFulfilled = (state, createdBall) => new State({
    ...state,
    all: [...state.all, new Ball(createdBall).toJS()]
}).toJS()
