import {combineEpics} from 'redux-observable'

import {articles} from 'src/articles/epics'
import {balls} from 'src/balls/epics'
import {rollers} from 'src/rollers/epics'

export const rootEpic = combineEpics(articles, balls, rollers)
