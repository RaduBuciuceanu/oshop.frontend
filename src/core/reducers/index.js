import {combineReducers} from 'redux'

import {articles} from 'src/articles/reducers'
import {balls} from 'src/balls/reducers'
import {rollers} from 'src/rollers/reducers'

export const rootReducer = combineReducers({articles, balls, rollers})
