import {applyMiddleware, createStore} from 'redux'
import {createEpicMiddleware} from 'redux-observable'

import {rootReducer} from 'src/core/reducers'
import {rootEpic} from 'src/core/epics'

class ReduxStoreBuilder {
    #epicMiddleware
    #history

    withHistory = (history) => {
        this.#history = history
        return this
    }

    build = () => {
        this.#epicMiddleware = this.#buildEpicMiddleware()
        return this.#buildSimpleStore()
    }

    #buildEpicMiddleware = () => {
        return createEpicMiddleware({
            dependencies: {
                history$: this.#history
            }
        })
    }

    #buildSimpleStore = () => {
        const store = createStore(rootReducer, applyMiddleware(this.#epicMiddleware))
        this.#epicMiddleware.run(rootEpic)
        return store
    }
}

export default ReduxStoreBuilder
