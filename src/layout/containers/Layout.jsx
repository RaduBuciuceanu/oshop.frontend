import React from 'react'
import {
  Divider,
  IconButton,
  Typography,
  AppBar,
  CssBaseline,
  Drawer,
  Hidden,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Toolbar,
  withStyles
} from '@material-ui/core'
import * as PropTypes from 'prop-types'
import InboxIcon from '@material-ui/icons/MoveToInbox'
import MenuIcon from '@material-ui/icons/Menu'
import { Switch, Route } from 'react-router-dom'

import CreateRollers from 'src/rollers/containers/Create'
import CreateBall from 'src/balls/containers/Create'
import AllArticles from 'src/articles/containers/All'

const drawerWidth = 240

const styles = theme => ({
  root: {
    display: 'flex',
  },
  drawer: {
    [theme.breakpoints.up('sm')]: {
      width: drawerWidth,
      flexShrink: 0,
    },
  },
  appBar: {
    marginLeft: drawerWidth,
    [theme.breakpoints.up('sm')]: {
      width: `calc(100% - ${drawerWidth}px)`,
    },
  },
  menuButton: {
    marginRight: theme.spacing(2),
    [theme.breakpoints.up('sm')]: {
      display: 'none',
    },
  },
  toolbar: theme.mixins.toolbar,
  drawerPaper: {
    width: drawerWidth,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
})

class Layout extends React.Component {
  state = { mobileOpen: false }

  render () {
    const { container, classes } = this.props
    const { mobileOpen } = this.state

    return <div className={classes.root}>
      <CssBaseline/>
      <AppBar position="fixed" className={classes.appBar}>
        <Toolbar>
          <IconButton color="inherit" aria-label="Open drawer" edge="start" onClick={this.#handleDrawerToggle}
                      className={classes.menuButton}>
            <MenuIcon/>
          </IconButton>
          <Typography variant="h6" noWrap>OShop</Typography>
        </Toolbar>
      </AppBar>
      <nav className={classes.drawer} aria-label="Mailbox folders">
        <Hidden smUp implementation="css">
          <Drawer
            container={container}
            variant="temporary"
            anchor={'left'}
            open={mobileOpen}
            onClose={this.#handleDrawerToggle}
            classes={{
              paper: classes.drawerPaper,
            }}
            ModalProps={{
              keepMounted: true,
            }}
          >
            {this.#drawer()}
          </Drawer>
        </Hidden>
        <Hidden xsDown implementation="css">
          <Drawer
            classes={{
              paper: classes.drawerPaper,
            }}
            variant="permanent"
            open
          >
            {this.#drawer()}
          </Drawer>
        </Hidden>
      </nav>
      <main className={classes.content}>
        <div className={classes.toolbar}/>
        <Switch>
          <Route exact path={'/rollers/create'} component={CreateRollers}/>
          <Route exact path={'/balls/create'} component={CreateBall}/>
          <Route exact path={'/articles/all'} component={AllArticles}/>
        </Switch>
      </main>
    </div>
  }

  #drawer = () => {
    const { classes } = this.props

    return <div>
      <div className={classes.toolbar}/>
      <Divider/>
      <List>
        <ListItem button onClick={() => this.#navigateTo('/articles/all')}>
          <ListItemIcon><InboxIcon/></ListItemIcon>
          <ListItemText primary={'All articles'}/>
        </ListItem>
      </List>
      <Divider/>
      <List>
        <ListItem button onClick={() => this.#navigateTo('/balls/create')}>
          <ListItemIcon><InboxIcon/></ListItemIcon>
          <ListItemText primary={'Create ball'} />
        </ListItem>
        <ListItem button onClick={() => this.#navigateTo('/rollers/create')}>
          <ListItemIcon><InboxIcon/></ListItemIcon>
          <ListItemText primary={'Create rollers'} />
        </ListItem>
      </List>
    </div>
  }

  #handleDrawerToggle = () => {
    this.setState({ mobileOpen: !this.state.mobileOpen })
  }

  #navigateTo = (link) => {
    this.props.history.replace(link)
  }
}

Layout.propTypes = {
  container: PropTypes.object,
  history: PropTypes.object.isRequired
}

export default withStyles(styles, { withTheme: true })(Layout)
