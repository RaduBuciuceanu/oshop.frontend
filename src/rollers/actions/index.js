import {CREATE_ROLLERS, CREATE_ROLLERS_FULFILLED} from 'src/rollers/actions/constants'

export const createRollers = (rollers) => ({type: CREATE_ROLLERS, payload: rollers})
export const createRollersFulfilled = (createdRollers) => ({type: CREATE_ROLLERS_FULFILLED, payload: createdRollers})
