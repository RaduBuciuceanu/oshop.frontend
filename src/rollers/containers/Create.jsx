import React from 'react'
import {TextField, Button, Grid, Typography} from '@material-ui/core'

class Create extends React.Component {
    render() {
        return <Grid container alignItems={'center'} justify={'center'} spacing={2}>
            <Grid item lg={7} md={12} sm={12} xs={12}>
                <Typography variant={'h4'} align={'center'}>Create rollers</Typography>
            </Grid>
            <Grid item lg={7} md={12} sm={12} xs={12}>
                <TextField label={'Name'} fullWidth/>
            </Grid>
            <Grid item lg={7} md={12} sm={12} xs={12}>
                <TextField label={'Description'} fullWidth/>
            </Grid>
            <Grid item lg={7} md={12} sm={12} xs={12}>
                <TextField label={'Price'} fullWidth/>
            </Grid>
            <Grid item lg={7} md={12} sm={12} xs={12}>
                <TextField label={'Wheel count'} fullWidth/>
            </Grid>
            <Grid item lg={7} md={12} sm={12} xs={12}>
                <TextField label={'Size'} fullWidth/>
            </Grid>
            <Grid item lg={7} md={12} sm={12} xs={12}>
                <Button fullWidth>Create</Button>
            </Grid>
        </Grid>
    }
}

export default Create
