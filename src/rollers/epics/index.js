import {ofType} from 'redux-observable'
import {switchMap, map} from 'rxjs/operators'
import {ajax} from 'rxjs/ajax'

import {CREATE_ROLLERS} from 'src/rollers/actions/constants'
import {createRollersFulfilled} from 'src/rollers/actions'

const url = 'http://localhost:5000/api/rollers/create'

const headers = {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
}

export const rollers = (actions$) => actions$.pipe(ofType(CREATE_ROLLERS))
    .pipe(switchMap(action => ajax.post(url, action.payload, headers)))
    .pipe(map((createdRollers) => createRollersFulfilled(createdRollers)))
