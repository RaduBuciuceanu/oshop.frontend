import {Record} from 'immutable'

import {CREATE_ROLLERS_FULFILLED} from 'src/rollers/actions/constants'

const Rollers = Record({id: null, name: null, description: null, price: null, wheelCount: null, size: null})
const State = Record({all: []})

export const rollers = (state = new State().toJS(), action) => {
    if (action.type === CREATE_ROLLERS_FULFILLED) {
        return createRollersFulfilled(state, action.payload)
    }

    return state
}

const createRollersFulfilled = (state, createdRollesrs) => new State({
    ...state,
    all: [...state.all, new Rollers(createdRollesrs).toJS()]
}).toJS()
